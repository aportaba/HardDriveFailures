library(survival)
library(cpm)
library(tidyverse)
library(lubridate)
library(zoo)
#Changepoint analysis


smartNamesFilteredHDD$date <- as.POSIXct(substr(smartNamesFilteredHDD$date,1,10), format = '%Y/%m/%d %H:%M:%S')
smartNamesFilteredHDD$date <- as.POSIXct(smartNamesFilteredHDD$date, format = '%Y/%m/%d %H:%M:%S')

smartNamesFilteredHDD <- smartNamesFilteredHDD[!is.na(smartNamesFilteredHDD$date)]
smartNamesFilteredHDD$date <- as.Date(smartNamesFilteredHDD$date)


# write_csv(smartNamesFiltered)
# z <- read.zoo("smartN.csv", format = "%Y/%m/%d", header=TRUE, tz="CEST", index.column = 2)


library(padr)

library(changepoint)
library(xts)


# Check if every day is available. If one day is missing, fill it with repeated data.
serialsFailed <- smartSuspects$Serial[smartSuspects$failed==1]
smartFailed <- smartSuspects[smartSuspects$Serial %in% serialsFailed]

a<- smartFailed[smartFailed$Vendor=="Seagate"]
a$date<- as.Date(a$date)

a <- a[,c("smart2","smart8","smart196","exp", "Vendor", "Version", "Device", "Port", "Model"):=NULL]

c<- as.data.frame(a)%>%
    pad(group = 'Serial', interval='day', by='date', break_above= 1500000000)



# Generate time series per Serial within the same Vendor, and eliminate every non-numeric value.

d <-c 

ts_df <-d%>%
  dplyr::group_by(Serial)%>% 
  xts(x =., order.by = .$date)

Serials <- unique(d$Serial)
# 
# x<- list()
# 
# for (j in 1:length(Serials)) {
# for (i in Serials) {
#   x[j]<-ts_df[ts_df$Serial==i]
#   }
# }

# We choose one in order to test the processing

serialsFailed <-unique(ts_df$Serial)

lista<-data.frame()

for (i in serialsFailed) {
    
e<-ts_df[ts_df$Serial==ts_df$Serial[[4]]]
e$Serial<-NULL

if(anyNA(e$smart198)){
lastNA <-as.Date(max(e$date[is.na(e$smart198)]))+1
}else{
  lastNA <-as.Date("2000-01-01")
}

lastDate <- as.Date(max(e$date))

e<-e[as.Date(as.POSIXct(e$date))>as.Date(lastNA)]
e$date<-NULL
e$host<-NULL

e[,(1:14)]<- as.numeric(e[,(1:14)])
e<-e[,(1:14)]

tsRainbow <- rainbow(ncol(e[,(1:(ncol(e)))]))

plot(x = as.numeric(e[,(1:14)]), plot.type = "single", ylab = "Smart metrics", main = "Smart metrics",
     col = tsRainbow, screens = 1)

# Set a legend in the upper left hand corner to match color to return series
legend(x = "topright", legend = names((e[,(1:(ncol(e)))])),
       lty = 1,col = tsRainbow)



elbowplot <- unlist(lapply(c(1:1000), function(p) cptfn(data = as.numeric(!is.na(e)), pen = p)))

plot(elbowplot)
title(main = "# changepoints for given penalty for x1")



mvalue<- cpt.meanvar(as.numeric(!is.na(e$smart198)),penalty="SIC",method="SegNeigh",class=TRUE)
mvalueOld<-mvalue

a<-merge(mvalue, mvalueOld)
setNames(object = a, i)

par(mfrow=c(1,1)) 

cpts(mvalue)

plot(mvalue)

}




library(PerformanceAnalytics)

par(mfrow=c(3, 1))
xts::plot.xts((e$smart198))
PerformanceAnalytics::chart.TimeSeries(((e)), order.by="time")

# We generate through k means a better Non Suspects dataset (similar size to Suspects)