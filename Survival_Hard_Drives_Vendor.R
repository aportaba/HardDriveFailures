
library(survival)
library(rms)
library(plyr)
library(data.table)
library(ggplot2)
library(stringr)
library(htmltools)
library(htmlwidgets)
library(lubridate)
library(tibble)
library(ggpubr)
library(PerformanceAnalytics)
library(RJSONIO)
library(rjson)
library(dplyr)


add_legend <- function(...) {
  opar <- par(fig=c(0, 1, 0, 1), oma=c(0, 0, 0, 0), 
    mar=c(0, 0, 0, 0), new=TRUE)
  on.exit(par(opar))
  plot(0, 0, type='n', bty='n', xaxt='n', yaxt='n')
  legend(...)
}

fit <- survfit(Surv(smart9/24,failed)~Vendor, data=smartNamesFiltered)


plot(fit,xlab = "Days", ylab = "Probability of survival", ylim = c(0.999,1), col=c(1:8))
add_legend("topright", names(fit$strata), col=(1:8),
   horiz=FALSE, cex=0.7, lwd=0.5, nc=5)

# Now we do the analysis for the old way of analysing disks (considering as failed those disks that disappear before the Last Available Day)

diskInfoDirk<- readLines("/home/aplocal/eos.disk.info")

SerialVendorInfoDirk <- matrix( c(0, 0), nrow=length(diskInfoDirk), ncol=2)


lastSerial <- "1"
lastVendor<- "1"
lastProduct <-"1"


wasLastVendor <-1
wasLastSerial <-1
counterVendor<-0


for(i in 1:length(diskInfoDirk)){
 if(wasLastSerial == 1){
    if(grepl("Vendor",diskInfoDirk[i]) == TRUE){
      wasLastVendor <-1
      wasLastSerial <-0

      counterVendor<- counterVendor+1

      SerialVendorInfoDirk[counterVendor,1] <-diskInfoDirk[i]
      SerialVendorInfoDirk[counterVendor,2] <- lastSerial
      
      lastVendor <- diskInfoDirk[i]
    }
    if(grepl("Serial number",diskInfoDirk[i]) == TRUE){
      wasLastVendor <-0
      wasLastSerial <-1
      
      lastSerial <- SerialVendorInfoDirk[counterVendor,2]
      lastVendor <- SerialVendorInfoDirk[counterVendor,1]
    }
  }
  else if(wasLastVendor == 1){
    if(grepl("Serial number",diskInfoDirk[i]) == TRUE){
      wasLastVendor <-0
      wasLastSerial <-1
      
      SerialVendorInfoDirk[counterVendor,1] <-lastVendor
      SerialVendorInfoDirk[counterVendor,2] <- diskInfoDirk[i]
      
      lastSerial <- diskInfoDirk[i]
    }
    if(grepl("Vendor",diskInfoDirk[i]) == TRUE){
      wasLastVendor <-1
      wasLastSerial <-0
      
      lastSerial <- SerialVendorInfoDirk[counterVendor,2]
      lastVendor <- SerialVendorInfoDirk[counterVendor,1]
    }
  }
}


SerialProductInfoDirk<- matrix( c(0, 0), nrow=length(diskInfoDirk), ncol=2)

wasLastProduct <-1
wasLastSerial <-1
counterProduct<-0

for(i in 1:length(diskInfoDirk)){
  if(wasLastSerial == 1){
    if(grepl("Product",diskInfoDirk[i]) == TRUE){
      wasLastProduct <-1
      wasLastSerial <-0
      
      counterProduct<- counterProduct+1
      
      SerialProductInfoDirk[counterProduct,1] <-diskInfoDirk[i]
      SerialProductInfoDirk[counterProduct,2] <- lastSerial
      
      lastProduct <- diskInfoDirk[i]
    }
    if(grepl("Serial number",diskInfoDirk[i]) == TRUE){
      wasLastProduct <-0
      wasLastSerial <-1
      
      lastSerial <- SerialProductInfoDirk[counterProduct,2]
      lastProduct <- SerialProductInfoDirk[counterProduct,1]
    }
  }
  else if(wasLastProduct == 1){
    if(grepl("Serial number",diskInfoDirk[i]) == TRUE){
      wasLastProduct <-0
      wasLastSerial <-1
      
      SerialProductInfoDirk[counterProduct,1] <-lastProduct
      SerialProductInfoDirk[counterProduct,2] <- diskInfoDirk[i]
      
      lastSerial <- diskInfoDirk[i]
    }
    if(grepl("Product",diskInfoDirk[i]) == TRUE){
      wasLastProduct <-1
      wasLastSerial <-0
      
      lastSerial <- SerialProductInfoDirk[counterProduct,2]
      lastProduct <- SerialProductInfoDirk[counterProduct,1]
    }
  }
}



SerialVendorInfoDirk<-SerialVendorInfoDirk[1:counterVendor,1:2]
SerialProductInfoDirk<-SerialProductInfoDirk[1:counterProduct,1:2]

SerialVendorInfoDirk[,1] <-gsub("Vendor:",  "", SerialVendorInfoDirk[,1])
SerialVendorInfoDirk[,2] <-gsub("Serial number:",  "", SerialVendorInfoDirk[,2])

SerialProductInfoDirk[,1] <-gsub("Product:",  "", SerialProductInfoDirk[,1])
SerialProductInfoDirk[,2] <-gsub("Serial number:",  "", SerialProductInfoDirk[,2])

colnames(SerialVendorInfoDirk) <- c("Vendor","Serial")
colnames(SerialProductInfoDirk) <- c("Model","Serial")


SerialVendorInfoDirk <- data.table(SerialVendorInfoDirk)
SerialProductInfoDirk <- data.table(SerialProductInfoDirk)

setkey(SerialVendorInfoDirk, "Serial")
setkey(SerialProductInfoDirk, "Serial")

SerialProductVendorInfoDirk <- SerialVendorInfoDirk[SerialProductInfoDirk]

SerialProductVendorInfoDirk$Serial <- toupper(SerialProductVendorInfoDirk$Serial)




# todaLaInfo <- join(infoOlof, SerialProductVendorInfoDirk, by="Serial", type ="full")
# todaLaInfo <- data.table(todaLaInfo)
# 
# smartF <- data.table(smartF)
# 
# #Setnames should only be used on the first iteration
# # setnames(smartF, "disk", "Serial")
# 
# smartF$Serial <- toupper(smartF$Serial)
# 
# setkey(smartF, "Serial")
# setkey(todaLaInfo, "Serial")
# 
# smartNamesDec17 <- smartF[todaLaInfo]
# 
# smartNamesFiltered <- smartNamesDec17[!is.na(smartNamesDec17$date)]
# smartNamesFiltered <- smartNamesFiltered[!is.null(smartNamesFiltered$date)]


fitPlot <- function(x){
  
  y <- setDT(x)[,.SD[which.max(date)],keyby=Serial]
  
  y[y$date == max(y$date)]$status <- 0
  
  fit <- survfit(Surv((smart9/24),sub)~1, data=y, start.time = (min(y$smart9)/24))
  
  plot(fit, xlab = "Days", ylab = "Probability of survival", yscale = 100, xlim = c((min(y$smart9)/24),(max(y$smart9)/24) ))
  
  }


tablePopularVendor <- smartNamesFiltered[,unique(.N),by=Vendor]

names(tablePopularVendor) <- c("Vendor", "NumberDisks")

setkey (tablePopularVendor$NumberDisks)

tablePopularVendor <- arrange(tablePopularVendor,desc(tablePopularVendor$NumberDisks))
tablePopularVendor <- data.table(tablePopularVendor)

helpTable <- matrix( c(0, 0), nrow=length(smartNamesFiltered$Vendor), ncol=2)

helpTable[,1]<- smartNamesFiltered$Vendor
helpTable[,2]<- smartNamesFiltered$Size

colnames(helpTable) <- c("Vendor","Size")

helpTable <- data.table(helpTable)
helpTable <- helpTable[!is.na(helpTable$Vendor)]
#helpTable$Size <- helpTable$Size[!is.na(helpTable$Size)]

setkey(helpTable, "Vendor")

helpTable<-subset(helpTable, !duplicated(Vendor))
helpTable<-data.table(helpTable)

tablePopularVendor<- data.table(tablePopularVendor)
tablePopularVendor <-left_join(tablePopularVendor,helpTable)

tablePopularVendor <- tablePopularVendor[!is.na(tablePopularVendor$Vendor),]


tablePopularVendor <- arrange(tablePopularVendor,desc(tablePopularVendor$NumberDisks))

print(tablePopularVendor)




NumberDisksMetaData <-length(unique(smartNamesFiltered$Serial))

NumberDisksSmart <- length(unique(smartF$Serial))

percentageWithMetaData <- NumberDisksSmart/NumberDisksMetaData




#We start by analyzing the most popular disk by different Serials that have been analyzed per Model

mostPopular <- data.table(tablePopularVendor[1, "Vendor"])
setnames(mostPopular, "V1", "Vendor") 
setkey(smartNamesFiltered, "Vendor")

mostPopularTable <- smartNamesFiltered[mostPopular, on="Vendor"]

mostPopularTable$status <- as.numeric(mostPopularTable$status)

mostPopularTable$status <- 1



# Second most popular disk

secondMostPopular <- data.table(tablePopularVendor[2, "Vendor"])
setnames(secondMostPopular, "V1", "Vendor") 
setkey(smartNamesFiltered, "Vendor")

secondMostPopularTable <- smartNamesFiltered[secondMostPopular, on="Vendor"]

secondMostPopularTable$status <- as.numeric(secondMostPopularTable$status)

secondMostPopularTable$status <- 1

# Third most popular disk

thirdMostPopular <- data.table(tablePopularVendor[3, "Vendor"])
setnames(thirdMostPopular, "V1", "Vendor")
setkey(smartNamesFiltered, "Vendor")

thirdMostPopularTable <- smartNamesFiltered[thirdMostPopular, on="Vendor"]

thirdMostPopularTable$status <- as.numeric(thirdMostPopularTable$status)

thirdMostPopularTable$status <- 1

# Fourth most popular disk

fourthMostPopular <- data.table(tablePopularVendor[4, "Vendor"])
setnames(fourthMostPopular, "V1", "Vendor")
setkey(smartNamesFiltered, "Vendor")

fourthMostPopularTable <- smartNamesFiltered[fourthMostPopular, on="Vendor"]

fourthMostPopularTable$status <- as.numeric(fourthMostPopularTable$status)

fourthMostPopularTable$status <- 1

# Fifth most popular disk

fifthMostPopular <- data.table(tablePopularVendor[5, "Vendor"])
setnames(fifthMostPopular, "V1", "Vendor")
setkey(smartNamesFiltered, "Vendor")

fifthMostPopularTable <- smartNamesFiltered[fifthMostPopular, on="Vendor"]

fifthMostPopularTable$status <- as.numeric(fifthMostPopularTable$status)

fifthMostPopularTable$status <- 1

# Sixth most popular disks

sixthMostPopular <- data.table(tablePopularVendor[6, "Vendor"])
setnames(sixthMostPopular, "V1", "Vendor")
setkey(smartNamesFiltered, "Vendor")

sixthMostPopularTable <- smartNamesFiltered[sixthMostPopular, on="Vendor"]

sixthMostPopularTable$status <- as.numeric(sixthMostPopularTable$status)

sixthMostPopularTable$status <- 1

# Seventh most popular disk

seventhMostPopular <- data.table(tablePopularVendor[7, "Vendor"])
setnames(seventhMostPopular, "V1", "Vendor")
setkey(smartNamesFiltered, "Vendor")

seventhMostPopularTable <- smartNamesFiltered[seventhMostPopular, on="Vendor"]

seventhMostPopularTable$status <- as.numeric(seventhMostPopularTable$status)

seventhMostPopularTable$status <- 1

# Eighth most popular disk

eighthMostPopular <- data.table(tablePopularVendor[8, "Vendor"])
setnames(eighthMostPopular, "V1", "Vendor")
setkey(smartNamesFiltered, "Vendor")

eighthMostPopularTable <- smartNamesFiltered[eighthMostPopular, on="Vendor"]

eighthMostPopularTable$status <- as.numeric(eighthMostPopularTable$status)

eighthMostPopularTable$status <- 1

# Ninth most popular disk

ninthMostPopular <- data.table(tablePopularVendor[9, "Vendor"])
setnames(ninthMostPopular, "V1", "Vendor")
setkey(smartNamesFiltered, "Vendor")

ninthMostPopularTable <- smartNamesFiltered[ninthMostPopular, on="Vendor"]

ninthMostPopularTable$status <- as.numeric(ninthMostPopularTable$status)

ninthMostPopularTable$status <- 1


#We choose per Vendor the latest date in which we have a measurment.
#Then we check the survival rates per Vendor


#Most popular:

fitPlot(mostPopularTable[!is.na(mostPopularTable$smart9)])

# Second most popular:

fitPlot(secondMostPopularTable[!is.na(secondMostPopularTable$smart9)])

# third most popular:

fitPlot(thirdMostPopularTable[!is.na(thirdMostPopularTable$smart9)])

# fourth most popular:

fitPlot(fourthMostPopularTable[!is.na(fourthMostPopularTable$smart9)])

# fifth most popular:

fitPlot(fifthMostPopularTable[!is.na(fifthMostPopularTable$smart9)])

# sixth most popular:

fitPlot(sixthMostPopularTable[!is.na(sixthMostPopularTable$smart9)])

# seventh most popular:

fitPlot(seventhMostPopularTable[!is.na(seventhMostPopularTable$smart9)])

# eighth most popular:

fitPlot(eighthMostPopularTable[!is.na(eighthMostPopularTable$smart9)])

# ninth most popular:

fitPlot(ninthMostPopularTable[!is.na(ninthMostPopularTable$smart9)])

#We combine all of them for plotting


mostPopularTableLastDate <- setDT(mostPopularTable)[,.SD[which.max(date)],keyby=Serial]
mostPopularTableLastDate$status <- 1
mostPopularTableLastDate[mostPopularTableLastDate$date == max(mostPopularTableLastDate$date)]$status <- 0

secondMostPopularTableLastDate <- setDT(secondMostPopularTable)[,.SD[which.max(date)],keyby=Serial]
secondMostPopularTableLastDate$status <- 1
secondMostPopularTableLastDate[secondMostPopularTableLastDate$date == max(secondMostPopularTableLastDate$date)]$status <- 0

thirdMostPopularTableLastDate <- setDT(thirdMostPopularTable)[,.SD[which.max(date)],keyby=Serial]
thirdMostPopularTableLastDate$status <- 1
thirdMostPopularTableLastDate[thirdMostPopularTableLastDate$date == max(thirdMostPopularTableLastDate$date)]$status <- 0

fourthMostPopularTableLastDate <- setDT(fourthMostPopularTable)[,.SD[which.max(date)],keyby=Serial]
fourthMostPopularTableLastDate$status <- 1
fourthMostPopularTableLastDate[fourthMostPopularTableLastDate$date == max(fourthMostPopularTableLastDate$date)]$status <- 0

fifthMostPopularTableLastDate <- setDT(fifthMostPopularTable)[,.SD[which.max(date)],keyby=Serial]
fifthMostPopularTableLastDate$status <- 1
fifthMostPopularTableLastDate[fifthMostPopularTableLastDate$date == max(fifthMostPopularTableLastDate$date)]$status <- 0

sixthMostPopularTableLastDate <- setDT(sixthMostPopularTable)[,.SD[which.max(date)],keyby=Serial]
sixthMostPopularTableLastDate$status <- 1
sixthMostPopularTableLastDate[sixthMostPopularTableLastDate$date == max(sixthMostPopularTableLastDate$date)]$status <- 0

seventhMostPopularTableLastDate <- setDT(seventhMostPopularTable)[,.SD[which.max(date)],keyby=Serial]
seventhMostPopularTableLastDate$status <- 1
seventhMostPopularTableLastDate[seventhMostPopularTableLastDate$date == max(seventhMostPopularTableLastDate$date)]$status <- 0

eighthMostPopularTableLastDate <- setDT(eighthMostPopularTable)[,.SD[which.max(date)],keyby=Serial]
eighthMostPopularTableLastDate$status <- 1
eighthMostPopularTableLastDate[eighthMostPopularTableLastDate$date == max(eighthMostPopularTableLastDate$date)]$status <- 0

ninthMostPopularTableLastDate <- setDT(ninthMostPopularTable)[,.SD[which.max(date)],keyby=Serial]
ninthMostPopularTableLastDate$status <- 1
ninthMostPopularTableLastDate[ninthMostPopularTableLastDate$date == max(ninthMostPopularTableLastDate$date)]$status <- 0

  
allLastDate <- rbind.fill(mostPopularTableLastDate,secondMostPopularTableLastDate) %>%
  rbind.fill(., thirdMostPopularTableLastDate) %>%
  rbind.fill(., fourthMostPopularTableLastDate) %>%
  rbind.fill(., fifthMostPopularTableLastDate) %>%
  rbind.fill(., sixthMostPopularTableLastDate)%>%
  rbind.fill(., seventhMostPopularTableLastDate)%>%
  rbind.fill(., eighthMostPopularTableLastDate)%>%
  rbind.fill(., ninthMostPopularTableLastDate)

allLastDate<-data.table(allLastDate)

remove(mostPopular)
remove(mostPopularTable)
remove(mostPopularTableLastDate)
remove(secondMostPopular)
remove(secondMostPopularTable)
remove(secondMostPopularTableLastDate)
remove(thirdMostPopular)
remove(thirdMostPopularTable)
remove(thirdMostPopularTableLastDate)
remove(fourthMostPopular)
remove(fourthMostPopularTable)
remove(fourthMostPopularTableLastDate)
remove(fifthMostPopular)
remove(fifthMostPopularTable)
remove(fifthMostPopularTableLastDate)
remove(sixthMostPopular)
remove(sixthMostPopularTable)
remove(sixthMostPopularTableLastDate)
remove(seventhMostPopular)
remove(seventhMostPopularTable)
remove(seventhMostPopularTableLastDate)
remove(eighthMostPopular)
remove(eighthMostPopularTable)
remove(eighthMostPopularTableLastDate)
remove(ninthMostPopular)
remove(ninthMostPopularTable)
remove(ninthMostPopularTableLastDate)

fit <- survfit(Surv(smart9/24,sub)~Vendor, data=allLastDate[allLastDate$Vendor!="Ata"])

plot(fit,xlab = "Days", ylab = "Probability of survival", ylim = c(0,1), col=c(1:8))
#legend(0,0.7, names(fit$strata), col=(1:9), lwd=0.5)
add_legend("topright", names(fit$strata), col=(1:9),
   horiz=FALSE, cex=0.7, lwd=0.5, nc=5)
